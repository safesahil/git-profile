import { all } from 'redux-saga/effects';

import appSagas from 'sagas/app';

export default function* rootSaga() {
  yield all([
    ...appSagas,
  ]);
}
