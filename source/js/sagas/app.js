import { takeLatest, call, put } from 'redux-saga/effects';

import api from 'api/app';
import {
    GET_GIT_PROFILE_REQUEST,
    getGitProfileError,
    getGitProfileSuccess,
    GET_GIT_PROFILE_REPOS_REQUEST,
    getGitProfileReposSuccess,
    getGitProfileReposError,
} from '../actions/app';

function getGitProfile() {
    return function* (action) {
        try {
            const username = action.username;
            const data = yield call(() => api.getUserProfile(username));
            yield put(getGitProfileSuccess(data));
        } catch (error) {
            yield put(getGitProfileError(error));
        }
    };
}

function getGitProfileRepos() {
    return function* (action) {
        try {
            const username = action.username;
            const data = yield call(() => api.getUserProfileRepos(username));
            yield put(getGitProfileReposSuccess(data));
        } catch (error) {
            yield put(getGitProfileReposError(error));
        }
    };
}

export function* appWatcher() {
    yield takeLatest(GET_GIT_PROFILE_REQUEST, getGitProfile());
    yield takeLatest(GET_GIT_PROFILE_REPOS_REQUEST, getGitProfileRepos());
}

export default [
    appWatcher(),
];
