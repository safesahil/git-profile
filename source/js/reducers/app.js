import { Map } from 'immutable';
import {
  GET_GIT_PROFILE_REQUEST,
  GET_GIT_PROFILE_SUCCESS,
  GET_GIT_PROFILE_ERROR,
  GET_GIT_PROFILE_REPOS_REQUEST,
  GET_GIT_PROFILE_REPOS_SUCCESS,
  GET_GIT_PROFILE_REPOS_ERROR
} from '../actions/app';

const initialState = Map({
  profileLoading: false,
  profile: null,
  profileError: '',

  reposLoading: false,
  repos: [],
  reposError: '',
});

const actionsMap = {
  [GET_GIT_PROFILE_REQUEST]: (state, action) => {
    return state.merge(Map({
      profileLoading: true,
      profile: null,
      profileError: '',
    }));
  },
  [GET_GIT_PROFILE_SUCCESS]: (state, action) => {
    let newState = { profileLoading: false };
    if (action.data && Object.keys(action.data).length > 0) {
      newState.profile = action.data;
    } else {
      newState.profileError = 'No user data found';
    }
    return state.merge(Map(newState));
  },
  [GET_GIT_PROFILE_ERROR]: (state, action) => {
    return state.merge(Map({
      profileLoading: false,
      profileError: action.error.toString(),
    }));
  },
  [GET_GIT_PROFILE_REPOS_REQUEST]: (state, action) => {
    return state.merge(Map({
      reposLoading: true,
      repos: [],
      reposError: '',
    }));
  },
  [GET_GIT_PROFILE_REPOS_SUCCESS]: (state, action) => {
    let newState = { reposLoading: false };
    if (action.data && action.data.length > 0) {
      newState.repos = action.data;
    } else {
      newState.reposError = 'No repositories found';
    }
    return state.merge(Map(newState));
  },
  [GET_GIT_PROFILE_REPOS_ERROR]: (state, action) => {
    return state.merge(Map({
      reposLoading: false,
      reposError: action.error.toString(),
    }));
  },
};

export default function reducer(state = initialState, action = {}) {
  const fn = actionsMap[action.type];
  return fn ? fn(state, action) : state;
}
