import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FaMapMarker, FaLayerGroup, FaEnvelopeOpen } from "react-icons/fa";
import { getGitProfileRequest, getGitProfileReposRequest } from '../actions/app';
import moment from "moment";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: '',
      language: '',
      search: '',
      reposState: [],
    };
  }


  componentWillMount() {
    const { dispatch } = this.props;
    let username = 'supreetsingh247';
    dispatch(getGitProfileRequest(username));
    dispatch(getGitProfileReposRequest(username));
  }

  render() {
    const { profile, repos } = this.props;
    const { reposState } = this.state;
    if (!profile) {
      return null;
    }
    return (
      <div className="profile-wrapper">
        <div className="pw-l">
          <div className="pw-l-head">
            <div className="img-wrapper">
              <img
                src={(profile && profile.avatar_url) ? profile.avatar_url : 'Testing'}
              />
            </div>
            <div className="p-title">
              <h1 className="vcard-names">
                <span className="p-name vcard-fullname">{(profile && profile.name) ? profile.name : 'Testing'}</span>
                <span className="p-nickname vcard-username">{(profile && profile.login) ? profile.login : 'Testing'}</span>
              </h1>
            </div>
            <div className="p-bio">
              <p>{(profile && profile.bio) ? profile.bio : 'Testing'}</p>
              <button type="button">Edit bio</button>
            </div>
          </div>
          <div className="pw-l-foot">
            <ul>
              <li><FaLayerGroup />{(profile && profile.company) ? profile.company : 'Testing'}</li>
              <li><FaMapMarker />{(profile && profile.location) ? profile.location : 'Testing'}</li>
              <li><FaEnvelopeOpen /><a>{(profile && profile.email) ? profile.email : 'Sign in to view email'}</a></li>
            </ul>
          </div>
        </div>
        <div className="pw-r">
          <div className="tab-wapper">
            <ul>
              <li><button>Overview</button></li>
              <li className="active"><button>Repositories <span className="badge-def">12</span></button></li>
              <li><button>Stars<span className="badge-def">7</span></button></li>
              <li><button>Followers<span className="badge-def">2</span></button></li>
              <li><button>Following<span className="badge-def">2</span></button></li>
            </ul>
          </div>
          <div className="Search-wrapper">
            <div className="sw-l">
              <input name="search" type="text" placeholder="Find a repository..." onChange={this.handleChange} />
            </div>
            <div className="sw-r">
              <div className="control-wrapper">
                <select name="language" id="language" onChange={this.handleChangeLanguage}>
                  <option value="">All</option>
                  <option value="html">HTML</option>
                  <option value="javascript">Javascript</option>
                  <option value="css">CSS</option>
                </select>
              </div>
              <div className="control-wrapper">
                <select name="type" id="type">
                  <option value="">All</option>
                  <option value="sources">Sources</option>
                  <option value="forks">Forks</option>
                  <option value="archived">Archived</option>
                  <option value="mirrors">Mirrors</option>
                </select>
              </div>
              <div className="control-wrapper">
                <button type="button">New</button>
              </div>
            </div>
          </div>
          <div className="repos-list-wrapper">
            {reposState && reposState.length > 0 &&
              <ul>
                {reposState.map((o, i) => (
                  <li key={i}>
                    <div className="rl-title">
                      <h1><a href="javascript:void(0)">{(o && o.name) ? o.name : ''}</a></h1>
                    </div>
                    <div className="rl-description rl-content">{(o && o.description) ? o.description : ''}</div>
                    <div className="rl-update-content rl-content">
                      {o && o.language &&
                        <span className="lang-color"></span>
                      }
                      {o && o.language &&
                        <span>{o.language}</span>
                      }
                      <span>{'Updated on ' + (o && o.updated_at) ? moment(o.updated_at).format('MMM DD, YYYY') : ''}</span>
                    </div>
                  </li>
                ))}
              </ul>
            }
          </div>
        </div>
      </div>
    );
  }

  componentDidUpdate(prevProps, prevState) {
    const { reposLoading, repos } = this.props;
    if (!reposLoading && prevProps.reposLoading !== reposLoading) {
      this.setState({ reposState: repos });
    }
  }

  handleChange = (e) => {
    const { name, value } = e.target;
    const { repos } = this.props;
    let newState = [];
    if (value && value.trim()) {
      repos.map((o, i) => {
        if (o.name && value) {
          let nm = o.name.trim().toLowerCase();
          let val = value.trim().toLowerCase();
          if (nm.search(val) >= 0) {
            newState.push(o);
          }
        }
      });
    } else {
      newState = repos;
    }
    this.setState({ reposState: newState, [name]: value });
  }

  handleChangeLanguage = (e) => {
    const { name, value } = e.target;
    const { repos } = this.props;
    let newState = [];
    if (value && value.trim()) {
      repos.map((o, i) => {
        if (o.language && value && o.language.trim().toLowerCase() === value.trim().toLowerCase()) {
          newState.push(o);
        }
      });
    } else {
      newState = repos;
    }
    this.setState({ reposState: newState, [name]: value });
  }

}

const mapStateToProps = (state) => {
  const { app } = state;
  return {
    profileLoading: app.get('profileLoading'),
    profile: app.get('profile'),
    profileError: app.get('profileError'),

    reposLoading: app.get('reposLoading'),
    repos: app.get('repos'),
    reposError: app.get('reposError'),
  };
}

export default connect(
  mapStateToProps,
)(Home);