export const GET_GIT_PROFILE_REQUEST = 'GET_GIT_PROFILE_REQUEST';
export const GET_GIT_PROFILE_SUCCESS = 'GET_GIT_PROFILE_SUCCESS';
export const GET_GIT_PROFILE_ERROR = 'GET_GIT_PROFILE_ERROR';

export const GET_GIT_PROFILE_REPOS_REQUEST = 'GET_GIT_PROFILE_REPOS_REQUEST';
export const GET_GIT_PROFILE_REPOS_SUCCESS = 'GET_GIT_PROFILE_REPOS_SUCCESS';
export const GET_GIT_PROFILE_REPOS_ERROR = 'GET_GIT_PROFILE_REPOS_ERROR';

export function getGitProfileRequest(username) {
    return {
        type: GET_GIT_PROFILE_REQUEST,
        username,
    }
}
export function getGitProfileSuccess(data) {
    return {
        type: GET_GIT_PROFILE_SUCCESS,
        data,
    }
}
export function getGitProfileError(error) {
    return {
        type: GET_GIT_PROFILE_ERROR,
        error,
    }
}

export function getGitProfileReposRequest(username) {
    return {
        type: GET_GIT_PROFILE_REPOS_REQUEST,
        username,
    }
}
export function getGitProfileReposSuccess(data) {
    return {
        type: GET_GIT_PROFILE_REPOS_SUCCESS,
        data,
    }
}
export function getGitProfileReposError(error) {
    return {
        type: GET_GIT_PROFILE_REPOS_ERROR,
        error,
    }
}