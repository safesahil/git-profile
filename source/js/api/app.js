import { get } from ".";

const requestUrl = 'users';

function getUserProfile(username) {
    return get(`${requestUrl}/${username}`);
}

function getUserProfileRepos(username) {
    return get(`${requestUrl}/${username}/repos`);
}

export default {
    getUserProfile,
    getUserProfileRepos,
}