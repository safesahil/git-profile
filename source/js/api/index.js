import axios from "axios";
import { APP_URL } from "../constants/constants";

export const get = (path, headers) => {
  const url = `${APP_URL}/${path}`;

  let defaultHeaders = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
  }

  let _headers = {
    ...defaultHeaders,
    ...headers
  }

  return axios({
    method: 'GET',
    url: url,
    headers: _headers
  }).then(function (res) {
    return res.data;
  }).catch(function (error) {
    if (error.response) {
      return error.response;
    } else {
      return error;
    }
  });
};